﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DemoIUpload.Dto;
using DemoIUpload.Entities;
using DemoIUpload.Mappers;
using DemoIUpload.Repos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DemoIUpload.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookController : ControllerBase
    {

        private BookRepository repo;

        public BookController(BookRepository repo)
        {
            this.repo = repo;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(repo.GetAll().Select(x => x.MapToDto()));
        }

        [HttpPost]
        public IActionResult Post(BookFormDto form)
        {
            try
            {
                repo.Insert(form.MapToEntity());
                return Ok();
            }
            catch(Exception e)
            {
                return Problem(e.Message);
            }
        }

        [HttpGet("image/{id}")]
        public IActionResult Get(int id)
        {
            //Book entity = repo.Get(id);
            Book entity = repo.GetAll().FirstOrDefault(x => x.Id == id);
            if(entity == null || entity.ImageFile == null)
            {
                return NotFound();
            }
            else
            {
                return File(entity.ImageFile, entity.ImageMimeType);
            }
        }
    }
}
