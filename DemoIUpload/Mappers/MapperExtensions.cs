﻿using DemoIUpload.Dto;
using DemoIUpload.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoIUpload.Mappers
{
    public static class MapperExtensions
    {

        public static BookDto MapToDto(this Book entity)
        {
            return new BookDto
            {
                Id = entity.Id,
                Title = entity.Title,
                ImageUrl = entity.ImageFile != null ? "/Book/Image/" + entity.Id : null
            };
        }

        public static Book MapToEntity(this BookFormDto dto)
        {
            return new Book
            {
                Id = dto.Id,
                Title = dto.Title,
                ImageFile = dto.ImageFile,
                ImageMimeType = dto.ImageMimeType
            };
        }
    }
}
