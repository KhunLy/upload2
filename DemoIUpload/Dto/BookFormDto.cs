﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoIUpload.Dto
{
    public class BookFormDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public byte[] ImageFile { get; set; }
        public string ImageMimeType { get; set; }
    }
}
