﻿using DemoIUpload.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace DemoIUpload.Repos
{
    public class BookRepository
    {
        private SqlConnection conn;

        public BookRepository(SqlConnection conn)
        {
            this.conn = conn;
        }

        public void Insert(Book entity)
        {
            conn.Open();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = "INSERT INTO Book(Title, ImageFile, ImageMimeType)";
            cmd.CommandText += " VALUES(@Title, @ImageFile, @ImageMimeType)";
            cmd.Parameters.AddWithValue("Title", entity.Title);
            cmd.Parameters.AddWithValue("ImageMimeType", (object)entity.ImageMimeType ?? DBNull.Value);
            cmd.Parameters.Add(new SqlParameter("ImageFile", SqlDbType.Binary) { Value = (object)entity.ImageFile ?? DBNull.Value });
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public IEnumerable<Book> GetAll()
        {
            conn.Open();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = "SELECT * FROM Book";
            SqlDataReader r = cmd.ExecuteReader();
            while(r.Read())
            {
                yield return new Book
                {
                    Id = (int)r["Id"],
                    Title = (string)r["Title"],
                    ImageFile = r["ImageFile"] as byte[],
                    ImageMimeType = r["ImageMimeType"] as string
                };
            }
            conn.Close();
        }
    }
}
